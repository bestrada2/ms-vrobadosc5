package com.promad.vrobadosc5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VrobadosC5Application {

    public static void main(String[] args) {
        SpringApplication.run(VrobadosC5Application.class, args);
    }

}
