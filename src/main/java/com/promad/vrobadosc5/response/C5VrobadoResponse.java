package com.promad.vrobadosc5.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class C5VrobadoResponse {

    private String message;
    private Boolean ok;
    private HttpStatus status;

    public C5VrobadoResponse(String message, Boolean ok, HttpStatus status) {
        this.message = message;
        this.ok = ok;
        this.status = status;
    }
}
