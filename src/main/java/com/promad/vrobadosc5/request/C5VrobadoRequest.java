package com.promad.vrobadosc5.request;


import lombok.Data;

@Data
public class C5VrobadoRequest {

    private String placa; 
    private String propietari;
    private String color;
    private String marca;
    private String submarca;
    private String modelo; 
    private String region;
    private String id; 
    private String fecha_rbo; 
    private String fecha_rec; 
    private String estatus; 
    private String region_rec; 
    private String id_rec; 
    private String edo; 
    private String lugar_robo; 
    private String registro; 
    private String hora_rbo; 
    private String av_previa;
    private String telefono;
    private Integer violencia;
    private String serie; 
    private String tipoveh;
    private String version;


    public C5VrobadoRequest(){}

}
