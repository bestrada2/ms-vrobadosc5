package com.promad.vrobadosc5.controller;

import com.promad.vrobadosc5.request.C5VrobadoRequest;
import com.promad.vrobadosc5.service.IC5VrobadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/c5")
public class C5VrobadosController {


    @Autowired
    private IC5VrobadoService ic5VrobadoService;

    @PostMapping("/vrobados")
    public ResponseEntity vrobados(@RequestBody C5VrobadoRequest vrobadoRequest) throws Exception {
        return ic5VrobadoService.newVrobado(vrobadoRequest);
    }


    @DeleteMapping("/vrobados/{placa}")
    public ResponseEntity vrobados(@PathVariable String placa) throws Exception {
        return ic5VrobadoService.deleteVrobado(placa);
    }

    @PutMapping("/vrobados")
    public ResponseEntity vrobadosUpdt(@RequestBody C5VrobadoRequest vrobadoRequest) throws Exception {
        return ic5VrobadoService.updateVrobado(vrobadoRequest);
    }

    @GetMapping("/vrobados/{placa}")
    public ResponseEntity vrobadoByPlaca(@PathVariable String placa) throws Exception {
        return ic5VrobadoService.vrobadoByPlaca(placa);
    }
}
