package com.promad.vrobadosc5.service;

import com.promad.vrobadosc5.request.C5VrobadoRequest;
import org.springframework.http.ResponseEntity;

public interface IC5VrobadoService {

    public ResponseEntity newVrobado(C5VrobadoRequest vrobadoRequest) throws Exception;

    public ResponseEntity deleteVrobado(String placa) throws Exception;

    public ResponseEntity updateVrobado(C5VrobadoRequest vrobadoRequest) throws Exception;

    public ResponseEntity vrobadoByPlaca(String plava) throws Exception;
}
