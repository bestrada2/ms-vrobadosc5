package com.promad.vrobadosc5.service;

import com.promad.vrobadosc5.request.C5VrobadoRequest;
import com.promad.vrobadosc5.response.C5VrobadoResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Service
public class IC5VrobadoslServiceImpl implements IC5VrobadoService {

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String usernameUrl;

    @Value("${spring.datasource.password}")
    private String passwordUrl;

    @Override
    public ResponseEntity newVrobado(C5VrobadoRequest vrobadoRequest) throws Exception {
        try {
            Connection connection = DriverManager.getConnection(url, usernameUrl, passwordUrl);

            PreparedStatement ps = connection.prepareStatement("SELECT * FROM vrobados_test WHERE placa = ?");
            ps.setString(1, vrobadoRequest.getPlaca());

            ResultSet rs = ps.executeQuery();


            if (rs.next()) {
                rs.close();
                ps.close();
                connection.close();
                return new ResponseEntity<>(new C5VrobadoResponse("That plate has already been registered", false, HttpStatus.FOUND), HttpStatus.FOUND);
            }

            ps.close();
            rs.close();

            ps = connection
                    .prepareStatement("insert into vrobados_test values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            ps.setString(1, vrobadoRequest.getPlaca());
            ps.setString(2, vrobadoRequest.getPropietari());
            ps.setString(3, vrobadoRequest.getColor());
            ps.setString(4, vrobadoRequest.getMarca());
            ps.setString(5, vrobadoRequest.getSubmarca());
            ps.setString(6, vrobadoRequest.getModelo());
            ps.setString(7, vrobadoRequest.getRegion());
            ps.setString(8, vrobadoRequest.getId());
            ps.setString(9, vrobadoRequest.getFecha_rbo());
            ps.setString(10, vrobadoRequest.getFecha_rec());
            ps.setString(11, vrobadoRequest.getEstatus());
            ps.setString(12, vrobadoRequest.getRegion_rec());
            ps.setString(13, vrobadoRequest.getId_rec());
            ps.setString(14, vrobadoRequest.getEdo());
            ps.setString(15, vrobadoRequest.getLugar_robo());
            ps.setString(16, vrobadoRequest.getRegistro());
            ps.setString(17, vrobadoRequest.getHora_rbo());
            ps.setString(18, vrobadoRequest.getAv_previa());
            ps.setString(19, vrobadoRequest.getTelefono());
            ps.setInt(20, vrobadoRequest.getViolencia());
            ps.setString(21, vrobadoRequest.getSerie());
            ps.setString(22, vrobadoRequest.getTipoveh());
            ps.setString(23, vrobadoRequest.getVersion());

            int affectedRows = ps.executeUpdate();

            ps.close();
            connection.close();

            if (affectedRows > 0)
                return new ResponseEntity<>(new C5VrobadoResponse("Inserted", true, HttpStatus.CREATED), HttpStatus.CREATED);


            return new ResponseEntity<>(new C5VrobadoResponse("Error, register cannot be inserted", false, HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

    }

    @Override
    public ResponseEntity deleteVrobado(String placa) throws Exception {
        try {
            Connection connection = DriverManager.getConnection(url, usernameUrl, passwordUrl);

            PreparedStatement ps = connection.prepareStatement("delete from vrobados_test where placa = ?;");

            ps.setString(1, placa);

            int affectedRows = ps.executeUpdate();

            if (affectedRows > 0)
                return new ResponseEntity<>(new C5VrobadoResponse("Record deleted", true, HttpStatus.FOUND), HttpStatus.FOUND);


            ps.close();
            connection.close();
            return new ResponseEntity<>(new C5VrobadoResponse("Record not found", false, HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public ResponseEntity updateVrobado(C5VrobadoRequest vrobadoRequest) throws Exception {
        try {
            Connection connection = DriverManager.getConnection(url, usernameUrl, passwordUrl);

            PreparedStatement ps = connection.prepareStatement("SELECT * FROM vrobados_test WHERE placa = ?");
            ps.setString(1, vrobadoRequest.getPlaca());

            ResultSet rs = ps.executeQuery();


            if (!rs.next()) {
                rs.close();
                ps.close();
                connection.close();
                return new ResponseEntity<>(new C5VrobadoResponse("That plate doesn't exist", false, HttpStatus.FOUND), HttpStatus.FOUND);
            }

            ps.close();
            rs.close();

            ps = connection
                    .prepareStatement("update vrobados_test " +
                            "set placa=?, propietari=?, color=?, marca=?, " +
                            "submarca=?, modelo=?, region=?, id=?, fecha_rbo=?, fecha_rec=?," +
                            "estatus=?, region_rec=?, id_rec=?, edo=?, lugar_robo=?, registro=?," +
                            "hora_rbo=?, av_previa=?, telefono=?, violencia=?, serie=?, tipoveh=?, version=? where placa=?");

            ps.setString(1, vrobadoRequest.getPlaca());
            ps.setString(2, vrobadoRequest.getPropietari());
            ps.setString(3, vrobadoRequest.getColor());
            ps.setString(4, vrobadoRequest.getMarca());
            ps.setString(5, vrobadoRequest.getSubmarca());
            ps.setString(6, vrobadoRequest.getModelo());
            ps.setString(7, vrobadoRequest.getRegion());
            ps.setString(8, vrobadoRequest.getId());
            ps.setString(9, vrobadoRequest.getFecha_rbo());
            ps.setString(10, vrobadoRequest.getFecha_rec());
            ps.setString(11, vrobadoRequest.getEstatus());
            ps.setString(12, vrobadoRequest.getRegion_rec());
            ps.setString(13, vrobadoRequest.getId_rec());
            ps.setString(14, vrobadoRequest.getEdo());
            ps.setString(15, vrobadoRequest.getLugar_robo());
            ps.setString(16, vrobadoRequest.getRegistro());
            ps.setString(17, vrobadoRequest.getHora_rbo());
            ps.setString(18, vrobadoRequest.getAv_previa());
            ps.setString(19, vrobadoRequest.getTelefono());
            ps.setInt(20, vrobadoRequest.getViolencia());
            ps.setString(21, vrobadoRequest.getSerie());
            ps.setString(22, vrobadoRequest.getTipoveh());
            ps.setString(23, vrobadoRequest.getVersion());
            ps.setString(24, vrobadoRequest.getPlaca());

            int affectedRows = ps.executeUpdate();

            ps.close();
            connection.close();


            if (affectedRows > 0)
                return new ResponseEntity<>(new C5VrobadoResponse("Record updated", true, HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);


            return new ResponseEntity<>(new C5VrobadoResponse("Error, record cannot be updated", false, HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public ResponseEntity vrobadoByPlaca(String placa) throws Exception {
        try {

            Connection connection = DriverManager.getConnection(url, usernameUrl, passwordUrl);

            PreparedStatement ps = connection.prepareStatement("SELECT * FROM vrobados_test WHERE placa = ?");

            ps.setString(1, placa);


            ResultSet rs = ps.executeQuery();

            if (!rs.next()) {
                ps.close();
                rs.close();
                connection.close();
                return new ResponseEntity<>(new C5VrobadoResponse("That plate doesn't exist", false, HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
            }

            ps.close();
            rs.close();
            connection.close();
            return new ResponseEntity<>(new C5VrobadoResponse(rs.getString(1), true, HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
